<!doctype html>
<html>
<head>
</head>
<body>
<h4>ติดต่อลูกค้าด้วย</h4>
<p>ชื่อ : {{ $name ?? '' }}</p>
<p>อีเมล : {{ $email ?? '' }}</p>
<p>เบอร์โทร : {{ $phone ?? '' }}</p>
<p>บริการ : {{ $service ?? '' }}</p>
</body>
</html>
