<div class="footer-area">

    <!-- footer start-->
    <footer class="ts-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <!-- footer social end-->
                    <div class="footer-menu text-center mb-25">
                        <ul>
                            <li><a href="{{ url()->route('home') }}">หน้าหลัก</a></li>
                            <li><a href="{{ url()->route('contact') }}">ติดต่อเรา</a></li>
                        </ul>
                    </div><!-- footer menu end-->
                    <div class="copyright-text text-center">
                        <p>Copyright © 2020 Be first. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end-->
    <div class="BackTo">
        <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
    </div>

</div>
