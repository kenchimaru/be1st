<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

<!-- FontAwesome -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
<!-- Animation -->
<link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
<!-- magnific -->
<link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">
<!-- carousel -->
<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
<!-- isotop -->
<link rel="stylesheet" href="{{ asset('frontend/css/isotop.css') }}">
<!-- ico fonts -->
<link rel="stylesheet" href="{{ asset('frontend/css/xsIcon.css') }}">
<!-- Template styles-->
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<!-- Responsive styles-->
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
