<header id="header" class="header header-transparent">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <!-- logo-->
            <a class="navbar-brand" href="{{ url()->route('home') }}">
                <img src="{{ asset('frontend/images/logos/logo-alt.png') }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                            <li><a href="{{ url()->route('home') }}">หน้าหลัก</a></li>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url()->route('contact') }}">ติดต่อเรา</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div><!-- container end-->
</header>
