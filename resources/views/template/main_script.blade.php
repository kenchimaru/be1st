<script src="{{ asset('frontend/js/jquery.js') }}"></script>

<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
<!-- Bootstrap jQuery -->
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<!-- Counter -->
<script src="{{ asset('frontend/js/jquery.appear.min.js') }}"></script>
<!-- Countdown -->
<script src="{{ asset('frontend/js/jquery.jCounter.js') }}"></script>
<!-- magnific-popup -->
<script src="{{ asset('frontend/js/jquery.magnific-popup.min.js') }}"></script>
<!-- carousel -->
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<!-- Waypoints -->
<script src="{{ asset('frontend/js/wow.min.js') }}"></script>

<!-- isotop -->
<script src="{{ asset('frontend/js/isotope.pkgd.min.js') }}"></script>

<!-- Template custom -->
<script src="{{ asset('frontend/js/main.js') }}"></script>
