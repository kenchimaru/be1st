@extends('template.app')
@section('content')

    <!-- banner start-->
    <section class="hero-area hero-speakers">
        <div class="banner-item overlay" style="background-image:url( {{ asset('frontend/images/hero_area/banner7.jpg') }} )">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="banner-content-wrap">

                            <p class="banner-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">Be first Be forward</p>
                            <h1 class="banner-title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="700ms">ก้าวสู่ความเป็น
                                ที่ 1 <br>กับ บีเฟิร์ส</h1>

                        </div>
                        <!-- Banner content wrap end -->
                    </div><!-- col end-->
                    <div class="col-lg-4 offset-lg-1">
                        <div class="hero-form-content">
                            <h2>รับคำปรึกษาฟรี</h2>
                            <p>
                                ติดต่อสอบถามบริการ <br/>ปรึกษากลยุททางการตลาด
                            </p>
                            <form action="{{ route('mailme') }}" method="POST" class="hero-form">
                                @csrf
                                <input class="form-control form-control-name" placeholder="ชื่อ" name="name" id="f-name"
                                       type="text" required="">
                                <input class="form-control form-control-phone" placeholder="เบอร์โทร" name="phone" id="f-phone"
                                       type="number">
                                <input class="form-control form-control-email" placeholder="อีเมล" name="email" id="f-email"
                                       type="email" required="">

                                <select name="service" id="service">
                                    <option value="service">บริการ</option>
                                    <option value="ส่งเสริมการตลาด / ยิงแอด">ส่งเสริมการตลาด / ยิงแอด</option>
                                    <option value="ทำเว็บ / แอปพลิเคชั่น Android iOS">ทำเว็บ / แอปพลิเคชั่น Android iOS</option>
                                    <option value="กราฟฟิคดีไซน์ / ภาพ / วีดีโอ">กราฟฟิคดีไซน์ / ภาพ / วีดีโอ</option>
                                </select>

                                <button class="btn" type="submit"> ติดต่อเลย</button>

                            </form><!-- form end-->
                        </div><!-- hero content end-->
                    </div><!-- col end-->
                </div><!-- row end-->
            </div>
            <!-- Container end -->
        </div>
    </section>
    <!-- banner end-->

    <!-- ts intro start -->

    <section class="ts-intro-item section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="300ms">
                    <div class="intro-left-content">
                        <h2 class="column-title">
                            <span>Be first Be forward</span>
                            ทำไมต้อง Be First
                        </h2>
                        <p>
                            หากคุณต้องการเป็นเบอร์หนึ่งในวงการ งานขายไม่ได้หยุดมือ ลูกค้าเข้าไม่หยุด ไว้ใจให้เราบริการคุณ
                        </p>
                    </div>
                </div><!-- col end-->
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                            <div class="single-intro-text mb-30">
                                <i class="icon icon-star"></i>
                                <h3 class="ts-title">จัดงานอีเวนท์</h3>
                                <p>
                                    รับจัดงานอีเวนท์เพื่อส่งเสริมการยอดขาย หาลูกค้าออฟไลน์ ต่อยอดงานออนไลน์ สร้างความน่าเชื่อถือต่อฐานลูกค้า
                                </p>
                                <span class="count-number">01</span>
                            </div><!-- single intro text end-->
                        </div><!-- col end-->
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">
                            <div class="single-intro-text mb-30">
                                <i class="icon icon-smartphone"></i>
                                <h3 class="ts-title">แอปพลิเคชั่น</h3>
                                <p>
                                    เพิ่มความสะดวกในการใช้บริการสำหรับลูกค้าคนสำคัญของคุณด้วยแอปพลิเคชั่นต่าง ๆ ไม่ว่าจะเว็บไซต์ แอนดรอย หรือ iOS
                                </p>
                                <span class="count-number">02</span>
                            </div><!-- single intro text end-->

                        </div><!-- col end-->
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="600ms">
                            <div class="single-intro-text mb-30">
                                <i class="icon icon-internet"></i>
                                <h3 class="ts-title">การตลาดออนไลน์</h3>
                                <p>
                                    ยิงแอดตรงกลุ่มเป้าหมาย หาลูกค้าในวงกว้าง ด้วยทีมงานการตลาดออนไลน์มืออาชีพ
                                </p>
                                <span class="count-number">03</span>
                            </div><!-- single intro text end-->
                        </div><!-- col end-->
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="700ms">
                            <div class="single-intro-text mb-30">
                                <i class="icon icon-fun"></i>
                                <h3 class="ts-title">สื่อโฆษณา</h3>
                                <p>
                                    งานภาพ งานเสียง หรือ ภาพเคลื่อนไหว ส่งเสริมการตลาดของคุณด้วยสื่อที่ร่าสนใจที่ใครเห็นก็ต้องหยุดดู
                                </p>
                                <span class="count-number">04</span>
                            </div><!-- single intro text end-->
                        </div><!-- col end-->
                    </div>
                </div><!-- col end-->

            </div><!-- row end-->
        </div><!-- container end-->
    </section>
    <!-- ts intro end-->
    <!-- ts experience start-->
    <section id="ts-experiences" class="ts-experiences">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 no-padding">
                    <div class="exp-img" style="background-image:url( {{ asset('frontend//images/cta_img.jpg') }} )">
                        <!-- <img class="img-fluid" src="images/cta_img.jpg" alt=""> -->
                    </div>
                </div><!-- col end-->
                <div class="col-lg-6 no-padding align-self-center wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="500ms">
                    <div class="ts-exp-wrap">
                        <div class="ts-exp-content">
                            <h2 class="column-title">
                                <span>รับประสบการณ์ใหม่</span>
                                เปลี่ยนมุมมองที่คุณมีต่อดิจิทัลมาเกตติ้ง
                            </h2>
                            <p>
                                ก้าวสู่ความเป็นผู้นำด้วยในวงการ เครื่องมือที่มีคุณภาพ และทีมงานผู้ชำนาญการ ที่จะพาคุณไปสู่ความสำเร็จที่คุณอาจคาดไม่ถึง
                            </p>
                        </div>
                    </div>

                </div><!-- col end-->
            </div><!-- row end-->
        </div><!-- container fluid end-->
    </section>
    <!-- ts experience end-->

    <!-- ts speaker start-->
    <section id="ts-speakers" class="ts-speakers" style="background-image:url( {{ asset('frontend/images/speakers/speaker_bg.png') }} )">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2 class="section-title text-center">
                        <span>ขอบคุณที่ไว้ใจให้เราดูแล</span>
                        ลูกค้าของเรา
                    </h2>
                </div><!-- col end-->
            </div><!-- row end-->
            <div class="row">
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/24th.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">24th Clinic</a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/beauty.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">BEAUTY BLOGS8</a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="600ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/mwatch.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">M Watch</a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="700ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/nid durian.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">น้องนิดทุเรียน <br>ตลาด อตก.</a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="800ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/ยาหม่อง.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">ยาหม่องตราช้าง</a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="900ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/richest.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">Richest </a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1000ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/ประตูหน้าต่าง.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">Trade Upvc </a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1100ms">
                    <div class="ts-speaker">
                        <div class="speaker-img">
                            <img class="img-fluid" src="{{ asset('frontend/images/our_client/น้ำหอม.jpg') }}" alt="">
                        </div>
                        <div class="ts-speaker-info">
                            <h3 class="ts-title"><a href="#">himM Men perfume </a></h3>
                        </div>
                    </div>
                </div> <!-- col end-->
            </div><!-- row end-->
        </div><!-- container end-->

        <!-- shap img-->
        <div class="speaker-shap">
            <img class="shap1" src="{{ asset('frontend/images/shap/home_speaker_memphis1.png') }}" alt="">
            <img class="shap2" src="{{ asset('frontend/images/shap/home_speaker_memphis2.png') }}" alt="">
            <img class="shap3" src="{{ asset('frontend/images/shap/home_speaker_memphis3.png') }}" alt="">
        </div>
        <!-- shap img end-->
    </section>

@endsection
