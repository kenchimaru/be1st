@extends('template.app')
@section('content')

    <div id="page-banner-area" class="page-banner-area" style="background-image:url({{ asset('frontend/images/hero_area/banner_bg.jpg') }})">
        <!-- Subpage title start -->
        <div class="page-banner-title">
            <div class="text-center">
                <h2>ติดต่อเรา</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">บีเฟิร์ส /</a>
                    </li>
                    <li>
                        ช่องทางการติดต่อ
                    </li>
                </ol>
            </div>
        </div><!-- Subpage title end -->
    </div><!-- Page Banner end -->

    <!-- ts intro start -->
    <section class="ts-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2 class="section-title text-center">
                        <span>Get Information</span>
                        ช่องทางการติดต่อ
                    </h2>
                </div><!-- col end-->
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-intro-text single-contact-feature">
                        <h3 class="ts-title">Facebook</h3>
                        <p>
                            <strong>Name:</strong> Be First Marketing
                        </p>
                        <p>
                            <strong>Link:</strong> <a href="https://www.facebook.com/be1stmarketing" target="_blank">https://fb.me/be1stmarketing</a>
                        </p>
                        <span class="count-number fa fa-facebook"></span>
                    </div><!-- single intro text end-->
                    <div class="border-shap left"></div>
                </div><!-- col end-->
                <div class="col-lg-4">
                    <div class="single-intro-text single-contact-feature">
                        <h3 class="ts-title">Line</h3>
                        <p>
                            <strong>Name:</strong> Be First Marketing
                        </p>
                        <p>
                            <strong>Link:</strong> <a href="https://lin.ee/pZlG5h9" target="_blank">https://lin.ee/pZlG5h9</a>
                        </p>
                        <span class="count-number fa fa-comment"></span>
                    </div><!-- single intro text end-->
                    <div class="border-shap left"></div>
                </div><!-- col end-->
                <div class="col-lg-4">
                    <div class="single-intro-text single-contact-feature">
                        <h3 class="ts-title">Email</h3>
                        <p>
                            <strong>Name:</strong> Be First Marketing
                        </p>
                        <p>
                            <strong>Email:</strong> <a href="mailto:support@befirst.live" target="_blank">support@befirst.live</a>
                        </p>
                        <span class="count-number fa fa-envelope"></span>
                    </div><!-- single intro text end-->
                    <div class="border-shap left"></div>
                </div><!-- col end-->

            </div><!-- row end-->
        </div><!-- container end-->
        <div class="speaker-shap">
            <img class="shap2" src="images/shap/home_schedule_memphis1.png" alt="">
        </div>
    </section>
    <!-- ts contact end-->
@endsection
