@extends('template.app')
@section('content')
    <section id="main-container" class="main-container">
        <div class="container pt-5">

            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <div class="error-page text-center">
                        <div class="error-code">
                            <h3><strong>เราได้ส่งข้อมูลให้เจ้าหน้าที่แล้ว</strong></h3>
                        </div>
                        <div class="error-message">
                            <h3>ขอบคุณที่สนใจบริการของเรา</h3>
                        </div>
                        <div class="error-body">
                            ทางเราที่จะติดต่อกลับอย่างเร็วที่สุด <br>
                            <a href="{{ route('home') }}" class="btn">Back to Home Page</a>
                        </div>
                    </div>
                </div>
            </div><!-- Content row -->
        </div><!-- Container end -->
    </section><!-- Main container end -->
@endsection
