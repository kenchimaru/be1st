<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PleaseContact extends Mailable
{
    use Queueable, SerializesModels;

    private $info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payload)
    {
        $this->info = $payload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->info['name'];
        $email = $this->info['email'];
        $phone = $this->info['phone'];
        $service = $this->info['service'];

        return $this->view('mail.contact',
            compact(
            'name',
            'email',
            'phone',
            'service'
            ));
    }
}
