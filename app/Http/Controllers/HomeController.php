<?php

namespace App\Http\Controllers;

use App\Mail\PleaseContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    function index() {

        return view('welcome');
    }

    function submitForm(Request $request) {

        $info['email'] = $request->email;
        $info['phone'] = $request->phone;
        $info['name'] = $request->name;
        $info['service'] = $request->service;
        Mail::to('chang.amarin@gmail.com')->queue(new PleaseContact($info));
        return view('submit_success');
    }
}
